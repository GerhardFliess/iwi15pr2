package netowking.lv.pingpong;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class PingPongClient {
	public static void main(String[] args) {

		BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

		Socket clientSocket = null;
		try {
			clientSocket = new Socket("localhost", 3333);

			BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
			BufferedReader socketRead = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			String consoleLine = consoleReader.readLine();
			while (consoleLine != null) {
				socketWriter.write(consoleLine);
				socketWriter.newLine();
				socketWriter.flush();
				String socketLine = socketRead.readLine();
				System.out.println(socketLine);
				consoleLine = consoleReader.readLine();
			}

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
