package netowking.lv.pingpong;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class PingPongServer {

	public static void main(String[] args) {

		// HashMap<String, Integer> map = new HashMap<String, Integer>();
		//
		// Set<String> keySet = map.keySet();
		//

		String line = "GET hansi";
		String name = parseName(line);

		ServerSocket serversocket = null;
		try {
			serversocket = new ServerSocket(3333);

			while (true) {
				// blocking
				Socket acceptedSocket = serversocket.accept();
				System.out.println("socket accepted");

				processClient(acceptedSocket);
			}

		} catch (ServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (serversocket != null)
					serversocket.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}

	private static String parseName(String line) {

		String[] split = line.split(" ");
		if (split.length == 2)
			return split[1];

		return null;
	}

	private static void processClient(Socket socket) throws ServerException {

		InputStreamReader reader;
		try {
			reader = new InputStreamReader(socket.getInputStream());
			BufferedReader socketReader = new BufferedReader(reader);

			BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

			String readLine = "";
			boolean error = false;
			readLine = socketReader.readLine();

			while (!socket.isClosed() && socket.isConnected() && !error && readLine != null) {

				System.out.println("gelesen:" + readLine);

				if (!socket.isClosed() && socket.isConnected()) {
					error = proccessAnswer(readLine, socketWriter);
				}
				readLine = socketReader.readLine();
			}
		} catch (IOException e) {
			throw new ServerException(e);
		}
	}

	private static boolean proccessAnswer(String readLine, BufferedWriter writer) {

		try {
			if (readLine == null) {
				writer.write("error");
			} else if (readLine.equals("ping")) {
				writer.write("pong");
			} else if (readLine.equals("pong")) {
				writer.write("ping");
			} else {
				writer.write("error");
			}
			writer.newLine();
			writer.flush();
			return true;
		} catch (IOException e) {

			return false;
		}

		// throw new IOException("test");

	}
}
