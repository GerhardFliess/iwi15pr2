package org.campus02.oop;

public class Landwirt extends Einwohner {

	public Landwirt(int flaeche, String bundesland) {
		super(flaeche * 100, bundesland);
	}

	@Override
	public double steuer() {

		return getEinkommen() * 0.225;
	}

}
