package org.campus02.oop;

import java.util.ArrayList;
import java.util.HashMap;

public class Bundesstaat {
	private ArrayList<Einwohner> einwohner = new ArrayList<Einwohner>();

	public void add(Einwohner temp) {
		einwohner.add(temp);
	}

	public double calcSteuer() {
		double result = 0;
		for (Einwohner einwohner2 : einwohner) {
			result += einwohner2.steuer();
		}
		return result;
	}

	public HashMap<String, Double> calcNachbundesland() {
		HashMap<String, Double> result = new HashMap<String, Double>();

		for (Einwohner temp : einwohner) {

			if (result.containsKey(temp.getBundesland())) {
				double bisherigeSteuer = result.get(temp.getBundesland());
				bisherigeSteuer = bisherigeSteuer + temp.steuer();

				result.put(temp.getBundesland(), bisherigeSteuer);

			} else {// erste einwohnerIn
				result.put(temp.getBundesland(), temp.steuer());
			}
		}

		return result;
	}
}
