package org.campus02.oop;

public abstract class Einwohner {
	private int einkommen;
	private String bundesland;

	public Einwohner(int einkommen, String bundesland) {
		this.einkommen = einkommen;
		this.bundesland = bundesland;
	}

	public abstract double steuer();

	public String getBundesland() {
		return bundesland;
	}

	public int getEinkommen() {
		return einkommen;
	}

	
}
