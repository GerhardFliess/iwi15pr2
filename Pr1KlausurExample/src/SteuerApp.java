import org.campus02.oop.Angestellter;
import org.campus02.oop.Bundesstaat;
import org.campus02.oop.Kuenstler;
import org.campus02.oop.Landwirt;

public class SteuerApp {

	public static void main(String[] args) {
		Kuenstler nitsch = new Kuenstler(40000, 1000000, "NOE");
		System.out.println(nitsch.steuer());

		Angestellter maxMustermann = new Angestellter(20000, 2, "ST");
		System.out.println(maxMustermann.steuer());

		Landwirt ottoNormalbauer = new Landwirt(1000, "NOE");
		System.out.println(ottoNormalbauer.steuer());

		Bundesstaat staat = new Bundesstaat();

		staat.add(nitsch);
		staat.add(maxMustermann);
		staat.add(ottoNormalbauer);

		System.out.println(staat.calcSteuer());

		System.out.println(staat.calcNachbundesland());

	}

}
