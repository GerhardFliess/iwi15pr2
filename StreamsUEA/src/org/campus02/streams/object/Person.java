package org.campus02.streams.object;

import java.io.Serializable;

public class Person implements Serializable {

	private static final long serialVersionUID = 1L;

	// transient // wird nicht gespeicher
	private String vorname;
	private String nachname;
	private int alter;

	public Person(String vorname, String nachname, int alter) {

		this.vorname = vorname;
		this.nachname = nachname;
		this.alter = alter;
	}

	@Override
	public String toString() {
		return "Person [vorname=" + vorname + ", nachname=" + nachname + ", alter=" + alter + "]";
	}

}
