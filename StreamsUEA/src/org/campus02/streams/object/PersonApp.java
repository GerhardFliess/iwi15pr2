package org.campus02.streams.object;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class PersonApp {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		
		Person p = new Person("Hansi", "Wurzl", 32);
		ObjectOutputStream oobjectOutStream = new ObjectOutputStream(new FileOutputStream("person.dat"));
		oobjectOutStream.writeObject(p);

		System.out.println(p);

		oobjectOutStream.flush();
		oobjectOutStream.close();

		ObjectInputStream objIn = new ObjectInputStream(new FileInputStream("person.dat"));
		Object readObject = objIn.readObject();
		System.out.println(readObject);
		Person gelesen =  (Person) readObject; //classe muss ich wissen...
		
		
	}
}
