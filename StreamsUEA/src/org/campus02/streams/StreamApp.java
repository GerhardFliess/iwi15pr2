package org.campus02.streams;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class StreamApp {
	public static void main(String[] args) {

		File outFile = new File("test.txt");
		System.out.println(outFile.exists());
		System.out.println(outFile.getAbsolutePath());
		System.out.println(outFile.getName());
		System.out.println(outFile.canWrite());

		try {
			writeText(outFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void writeText(File outFile) throws IOException {
		FileWriter writer = null;
		try {
			writer = new FileWriter(outFile);
			writer.write("Mimi spielt ball und mal Äpfel");
			writer.flush();
			return;
		} finally {
			writer.close();
			System.out.println("bye");
		}
	}
}
