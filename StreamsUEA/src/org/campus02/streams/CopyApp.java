package org.campus02.streams;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class CopyApp {
	public static void main(String[] args) throws IOException {

//		InputStream inputStream = new FileInputStream("test.txt");
		
		URL url = new URL("http://www.campus02.at");
		InputStream inputStream  = url.openStream();
		
		FileOutputStream fileOutStream = new FileOutputStream("copy.txt");

		
		int data;
		while ((data = inputStream.read()) != -1) {
			
			fileOutStream.write(data);
		}

		inputStream.close();
		
		fileOutStream.flush();
		fileOutStream.close();

	}
}
