package org.campus02.streams;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ConsoleReadApp {
	public static void main(String[] args) throws IOException {

		FileOutputStream fileOut = openStream();

		BufferedOutputStream buffered = new BufferedOutputStream(fileOut);

		int data = 0;
		// (data = System.in.read()) = Zuweiseung auf data
		// und vergleichen ob data != -1 --> Stream leer
		while ((data = System.in.read()) != -1) {
			if ('x' == data) {
				break;
			}
			buffered.write(data);
		}
		buffered.flush();
		buffered.close();

		// Reader in = new InputStreamReader(System.in); // textbasiert lesen
		// BufferedReader consoleReader = new BufferedReader(in); // zeilenweise
		// String line = consoleReader.readLine();
	}

	private static FileOutputStream openStream() throws FileNotFoundException {
		
		
		File outFile = new File("eingabe.txt");
		FileOutputStream fileOut = new FileOutputStream(outFile);
		return fileOut;
		
//		throw new IndexOutOfBoundsException("");
	}
}
