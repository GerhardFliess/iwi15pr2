package org.campus02.tippserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class TippServer implements Runnable {

	private static TippServerLogic logic = new TippServerLogic();

	private ServerSocket socket;
	private ArrayList<TippServerWorker> workers = new ArrayList<>();
	private boolean stopped = false;

	public void shutDown() {
		System.out.println("server stoppt");
		stopped = true;
		if (socket != null)
			try {
				socket.close();
			} catch (IOException e) {

			}

		for (TippServerWorker tippServerWorker : workers) {
			tippServerWorker.shutdown();
		}
	}

	@Override
	public void run() {

		try {
			logic.setNotifyer(this);

			socket = new ServerSocket(10000);

			while (true) {
				Socket clientSocket = socket.accept(); // bleibt hängen...
				System.out.println("new worker");
				TippServerWorker worker = new TippServerWorker(logic, clientSocket);
				workers.add(worker);

				Thread thread = new Thread(worker);
				thread.start();
			}

		} catch (IOException e) {
			if (!stopped)
				e.printStackTrace();
		}

	}
}
