package org.campus02.tippserver;

public class LogicTestApp {

	public static void main(String[] args) {
		TippServerLogic logic = new TippServerLogic();

		System.out.println(logic.serverStateName());
		System.out.println(logic.ask("2:1"));

		System.out.println(logic.put("gerhard 2:1"));

		System.out.println(logic.serverStateName());
		System.out.println(logic.ask("2:1"));
		System.out.println(logic.put("hans 2:1"));

		System.out.println(logic.serverStateName());
		System.out.println(logic.ask("2:1"));
	
		System.out.println(logic.put("karl 1:1"));

		System.out.println(logic.serverStateName());
		System.out.println(logic.ask("2:1"));
		System.out.println(logic.ask("1:1"));
		System.out.println(logic.ask("2:2"));

		System.out.println(logic.start());
		System.out.println(logic.put("otto 1:1"));

	
	}

}
