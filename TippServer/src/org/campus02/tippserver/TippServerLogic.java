package org.campus02.tippserver;

import java.util.ArrayList;
import java.util.HashMap;

public class TippServerLogic {


	public enum State {
		before, running, stopped
	}

	private static final String OK = "OK";
	private static final String NOK = "NOK";

	private State serverState = State.before;
	
	private HashMap<String, ArrayList<String>> tipps = new HashMap<>();
	private TippServer notifyer;

	public TippServerLogic() {

	}
	
	public void setNotifyer(TippServer notifyer) {
		this.notifyer = notifyer;
	}

	public synchronized String put(String value) {

		String result = null;
		switch (serverState) {
		case before:
			result = handlePut(value);
			break;
		case running:
		case stopped:
			result = NOK;
			break;
		}
		return result;
	}

	private String handlePut(String value) {
		if (value == null || value.length() == 0)
			return NOK;

		String[] tokens = value.split(" ");
		if (tokens.length != 2)
			return NOK;

		ArrayList<String> list = tipps.get(tokens[1]);
		if (list == null) {
			list = new ArrayList<String>();
		}
		list.add(tokens[0]);
		tipps.put(tokens[1], list);

		return OK;
	}

	public String ask(String value) {

		ArrayList<String> arrayList = tipps.get(value);
		if (arrayList == null)
			return "[]";

		return arrayList.toString();
	}

	public String serverStateName() {
		return serverState.toString();
	}

	public String start() {
		if (serverState != State.before)
			return NOK;

		serverState = State.running;
		return OK;
	}

	public void shutDown() {
		if (serverState == State.stopped)
			if (notifyer != null)
				notifyer.shutDown();
	}

	public String stop() {
		String result = null;
		switch (serverState) {
		case running:
			serverState = State.stopped;
			result = OK;
			break;
		case before:
		case stopped:
			result = NOK;
			break;
		}
		return result;
	}

}
