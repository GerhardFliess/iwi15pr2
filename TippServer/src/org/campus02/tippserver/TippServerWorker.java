package org.campus02.tippserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class TippServerWorker implements Runnable {

	private TippServerLogic logic;
	private Socket socket;
	private boolean stopped = false;

	public TippServerWorker(TippServerLogic logic, Socket socket) {
		super();
		this.logic = logic;
		this.socket = socket;
	}

	@Override
	public void run() {
		InputStreamReader reader;
		try {
			reader = new InputStreamReader(socket.getInputStream());
			BufferedReader socketReader = new BufferedReader(reader);

			BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

			String readLine = "";
			boolean error = false;
			readLine = socketReader.readLine(); // Blockierend bis gelesen
												// werden kann oder
												// socket.close() aufgerufen
												// wird

			while (!socket.isClosed() && socket.isConnected() && !error && readLine != null) {
				System.out.println("read:" + readLine);
				if (readLine.startsWith("put ")) {
					String result = logic.put(readLine.substring(4));
					writeResult(socketWriter, result);
				} else if (readLine.startsWith("ask ")) {
					String result = logic.ask(readLine.substring(4));
					writeResult(socketWriter, result);
				} else if (readLine.startsWith("start")) {
					String result = logic.start();
					writeResult(socketWriter, result);
				} else if (readLine.startsWith("state")) {
					String result = logic.serverStateName();
					writeResult(socketWriter, result);
				} else if (readLine.startsWith("stop")) {
					String result = logic.stop();
					writeResult(socketWriter, result);
				} else if (readLine.contains("shutdown")) {
					logic.shutDown();
				}

				readLine = socketReader.readLine();
			}
		} catch (IOException e) {
			if (!stopped)
				e.printStackTrace();
		}
	}

	private void writeResult(BufferedWriter socketWriter, String result) throws IOException {
		socketWriter.write(result);
		socketWriter.newLine();
		socketWriter.flush();
	}

	public void shutdown() {
		System.out.println("worker stopped");
		stopped = true;
		try {
			socket.close();
		} catch (IOException e) {
		}
	}
}
