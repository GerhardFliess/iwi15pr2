

import java.io.IOException;

import org.campus02.tippserver.TippServer;

public class ServerApp {

	public static void main(String[] args) throws IOException {

		Thread server = new Thread(new TippServer());
		server.start();
	}

}
